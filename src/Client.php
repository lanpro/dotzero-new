<?php

namespace AmoCRM;

use AmoCRM\Helpers\Fields;
use AmoCRM\Helpers\Format;
use AmoCRM\Models\ModelInterface;
use AmoCRM\Request\CurlHandle;
use AmoCRM\Request\ParamsBag;

/**
 * Class Client
 *
 * Основной класс для получения доступа к моделям amoCRM API
 *
 * @package AmoCRM
 * @author dotzero <mail@dotzero.ru>
 * @link http://www.dotzero.ru/
 * @link https://github.com/dotzero/amocrm-php
 * @property \AmoCRM\Models\Account $account
 * @property \AmoCRM\Models\Call $call
 * @property \AmoCRM\Models\Catalog $catalog
 * @property \AmoCRM\Models\CatalogElement $catalog_element
 * @property \AmoCRM\Models\Company $company
 * @property \AmoCRM\Models\Contact $contact
 * @property \AmoCRM\Models\Customer $customer
 * @property \AmoCRM\Models\CustomersPeriods $customers_periods
 * @property \AmoCRM\Models\CustomField $custom_field
 * @property \AmoCRM\Models\Lead $lead
 * @property \AmoCRM\Models\Links $links
 * @property \AmoCRM\Models\Note $note
 * @property \AmoCRM\Models\Pipelines $pipelines
 * @property \AmoCRM\Models\Task $task
 * @property \AmoCRM\Models\Transaction $transaction
 * @property \AmoCRM\Models\Unsorted $unsorted
 * @property \AmoCRM\Models\Webhooks $webhooks
 * @property \AmoCRM\Models\Widgets $widgets
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Client
{
    /**
     * @var Fields|null Экземпляр Fields для хранения номеров полей
     */
    public $fields = null;

    /**
     * @var ParamsBag|null Экземпляр ParamsBag для хранения аргументов
     */
    public $parameters = null;

    /**
     * @var CurlHandle Экземпляр CurlHandle для повторного использования
     */
    private $curlHandle;

    /**
     * Client constructor
     *
     * @param  string  $domain  Поддомен или домен amoCRM
     * @param  string  $clientSecret  Секретный ключ
     * @param  string  $clientId  ID интеграции
     * @param  string  $tokenPath  Путь до токен файла
     * @param  string  $redirectUri  Redirect URI указанный в настройках интеграции
     * @param  string|null  $proxy  Прокси сервер для отправки запроса
     */
    public function __construct(
        $domain,
        $clientSecret,
        $clientId,
        $tokenPath,
        $redirectUri,
        $proxy = null
    ) {
        // Разернуть поддомен в полный домен
        if (strpos($domain, '.') === false) {
            $domain = sprintf('%s.amocrm.ru', $domain);
        }

        $this->parameters = new ParamsBag();
        $this->parameters->addAuth('domain', $domain);
        $this->parameters->addAuth('clientSecret', $clientSecret);
        $this->parameters->addAuth('clientId', $clientId);
        $this->parameters->addAuth('tokenPath', $tokenPath);
        $this->parameters->addAuth('redirectUri', $redirectUri);

        if ($proxy !== null) {
            $this->parameters->addProxy($proxy);
        }

        $this->fields = new Fields();

        $this->curlHandle = new CurlHandle();

        if (isset($_REQUEST['code'])) {
            $this->getAccessTokenByCode($_REQUEST['code']);
        } else {
            $this->getAccessToken($tokenPath);
        }
    }

    private function getAccessToken($tokenPath)
    {
        if (file_exists($tokenPath)) {
            if (filemtime($tokenPath) + 86400 < time() - 100) {
                $refreshToken = json_decode(file_get_contents($tokenPath), 1)['refresh_token'];
                $response = $this->getAccessTokenByRefreshToken($refreshToken);
                $this->parameters->addAuth('accessToken', $response['access_token']);
            } else {
                $this->parameters->addAuth(
                    'accessToken',
                    json_decode(file_get_contents($tokenPath), 1)['access_token']
                );
            }
        } else {
            $this->authorization();
        }
    }

    private function getAccessTokenByRefreshToken($refreshToken)
    {
        return $this->makeRequest(
            [
                'client_id' => $this->parameters->getAuth('clientId'),
                'client_secret' => $this->parameters->getAuth('clientSecret'),
                'grant_type' => 'refresh_token',
                'refresh_token' => $refreshToken,
                'redirect_uri' => $this->parameters->getAuth('redirectUri')
            ]
        );
    }

    private function makeRequest($data)
    {
        /** Формируем URL для запроса */
        $link = 'https://'.$this->parameters->getAuth('domain').'/oauth2/access_token';

        /**
         * Нам необходимо инициировать запрос к серверу.
         * Воспользуемся библиотекой cURL (поставляется в составе PHP).
         * Вы также можете использовать и кроссплатформенную программу cURL, если вы не программируете на PHP.
         */
        $curl = curl_init(); //Сохраняем дескриптор сеанса cURL

        /** Устанавливаем необходимые опции для сеанса cURL  */
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-oAuth-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        $out = curl_exec($curl); //Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        /** Теперь мы можем обработать ответ, полученный от сервера. Это пример. Вы можете обработать данные своим способом. */
        $code = (int)$code;

        $errors = [
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable',
        ];

        try {
            /** Если код ответа не успешный - возвращаем сообщение об ошибке  */
            if ($code < 200 || $code > 204) {
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undefined error', $code);
            }
        } catch (\Exception $e) {
            die('Ошибка: '.$e->getMessage().PHP_EOL.'Код ошибки: '.$e->getCode());
        }

        file_put_contents($this->parameters->getAuth('tokenPath'), $out);

        /**
         * Данные получаем в формате JSON, поэтому, для получения читаемых данных,
         * нам придётся перевести ответ в формат, понятный PHP
         */
        return json_decode($out, 1);
    }

    private function authorization()
    {
        $url = 'https://www.amocrm.ru/oauth?client_id='.$this->parameters->getAuth('clientId');
        header('Location: '.$url);
        die;
    }

    /**
     * Возвращает экземпляр модели для работы с amoCRM API
     *
     * @param  string  $name  Название модели
     * @return ModelInterface
     * @throws ModelException
     */
    public function __get($name)
    {
        $classname = '\\AmoCRM\\Models\\'.Format::camelCase($name);

        if (!class_exists($classname)) {
            throw new ModelException('Model not exists: '.$name);
        }

        // Чистим GET и POST от предыдущих вызовов
        $this->parameters->clearGet()->clearPost();

        return new $classname($this->parameters, $this->curlHandle);
    }

    private function getAccessTokenByCode($code)
    {
        $this->makeRequest(
            [
                'client_id' => $this->parameters->getAuth('clientId'),
                'client_secret' => $this->parameters->getAuth('clientSecret'),
                'grant_type' => 'authorization_code',
                'code' => $code,
                'redirect_uri' => $this->parameters->getAuth('redirectUri')
            ]
        );
    }
}
